<?php
  include('database.php');
  session_start();
  $records = $connection->prepare('SELECT id_cp,descripcion FROM  punto_control;');
  $records->execute();
  $control_points = $records->fetchAll();
?>
<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>Sistema de control de acceso FI UAEM</title>
  <!-- Custom fonts for this template-->
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"/>
  <link href="fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link rel="icon" type="image/png" href="favicon.png"/>
  <!-- Custom styles for this template-->
  <link href="css/standard-style.css" rel="stylesheet">
  <link rel="stylesheet" href="css/empleado-style.css">
</head>
<body id="page-top">
  <!-- Page Wrapper -->
  <div id="wrapper">
    <!-- Sidebar -->
    <ul class="navbar-nav sidebar accordion" id="accordionSidebar">
      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="">
        <div class="sidebar-brand-icon">
          <img src="img/png/uaem-logo.png" alt="Universidad Autonoma del Estado de Mexico" width="57px" height="50px" style="border-radius:3px;">
        </div>
        <div class="sidebar-brand-text mx-2">UAEM</div>
      </a>
      <!-- Divider -->
      <hr class="sidebar-divider my-0">
      <!-- Nav Item - Dashboard -->
      <li class="nav-item active">
        <a class="nav-link" href="empleado.php">
          <ion-icon name="contract-outline" style="font-size: 22px; vertical-align: middle; padding-right:5px;"></ion-icon>
          <span>Puntos de control</span>
        </a>
      </li>
      <!-- Divider -->
      <hr class="sidebar-divider">
      <!-- Heading -->
      <div class="sidebar-heading">
        Operaciones
      </div>
      <!-- Nav Item -->
      <li class="nav-item">
        <a class="nav-link" href="empleado-files/empleado-registrar.php">
        <ion-icon name="person-add-outline" style="font-size: 22px; vertical-align: middle; padding-right:5px;"></ion-icon>
          <span>Registrar acceso</span>
        </a>
      </li>
      <!-- Divider -->
      <hr class="sidebar-divider d-none d-md-block">
      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>
    </ul>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">
      <!-- Main Content -->
      <div id="content">

      <?php include($_SERVER['DOCUMENT_ROOT'].'/sistemaAccesoFI/templates/navbar.php'); ?>

        <!-- Begin Page Content -->
        <div class="container-fluid">

           <!-- Page Heading -->
           <div class="col-11 mx-auto">
            <h1 class="h3">Puntos de control</h1>
            <p>A continuacion se muestra la lista de los puntos de control disponibles. Podras descargar el QR de algun punto de control al hacer click sobre el.</p>
           </div>
           
           <div class="row mb-5">
          <?php for ($i = 0; $i < sizeof($control_points); $i++) {?>
            <div class="col-md-11 col-sm-12 my-2 mx-auto">
              <a href="empleado-files/empleado-cp.php?id_cp=<?= $control_points[$i]['id_cp']?>&cp_descripcion=<?= $control_points[$i]['descripcion']?>" class="card card-top card-hov shadow h-100" style="text-decoration:none">
                   <div class="card-body m-0 p-0 my-3 mx-3">
                     <div class="row no-gutters align-items-center m-0 p-0">
                       <div class="col-10">
                         <div class="card-top-title">ID: <?= $control_points[$i]['id_cp']?></div>
                         <div class="card-top-content"><?= $control_points[$i]['descripcion']?></div>
                       </div>
                       <div class="col-2 d-flex justify-content-end">
                        <button class="btn btn-card m-1"><ion-icon name="download-outline" style="font-size: 25px; vertical-align:middle;"></ion-icon></button>
                       </div>
                     </div>
                   </div>
               </a>
             </div>
             <?php };?>
            </div>
        </div>
        <!-- /.container-fluid -->
      </div>
      <!-- End of Main Content -->
    </div>
    <!-- End of Content Wrapper -->
  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <?php include('templates/logout-modal.php'); ?>

  <script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
  <script src="https://unpkg.com/ionicons@5.2.3/dist/ionicons.js"></script>
  <!-- Custom scripts for all pages-->
  <script src="js/standard-func.js"></script>
</body>
</html>
