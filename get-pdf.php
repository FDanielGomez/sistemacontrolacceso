<?php
    include('database.php');
    include('vendor/autoload.php');
    use Mpdf\Mpdf;
    $id_cp = $_GET['id_cp'];

    $records = $connection->prepare('SELECT img_text,descripcion FROM punto_control WHERE id_cp = :id_cp;');
    $records->bindParam('id_cp',$id_cp);
    $records->execute();
    $data = $records->fetch(PDO::FETCH_ASSOC);
    $imageData = $data['img_text'];
    $cp_descripcion = $data['descripcion'];

    $pdf = new Mpdf([]);
    $html = file_get_contents('templates/plantilla1.html');
    $html .= $cp_descripcion;
    $html.= file_get_contents('templates/plantilla2.html');
    $html .= $imageData;
    $html.= file_get_contents('templates/plantilla3.html');
    $css = file_get_contents('css/qr-pdf-style.css');

    $pdf->writeHtml($css,\Mpdf\HTMLParserMode::HEADER_CSS);
    $pdf->writeHtml($html,\Mpdf\HTMLParserMode::HTML_BODY);
    $pdf->Output("Punto_Control_".$id_cp.".pdf","D");
