<?php
    include('../database.php');
    $id_persona = $_POST['id_persona'];
    $records = $connection->prepare('SELECT id_persona,identificador,email,password,nombre FROM persona WHERE id_persona = :id_persona');
    $records->bindParam('id_persona',$id_persona);
    $records->execute();
    $result = $records->fetch(PDO::FETCH_ASSOC);
    echo json_encode($result);
?>
