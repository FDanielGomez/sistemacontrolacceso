<?php
    include('../database.php');
    $id_materia = $_POST['id_materia'];
    $records = $connection->prepare('DELETE FROM materia WHERE id_materia = :id_materia');
    $records->bindParam('id_materia',$id_materia);
    if( $records->execute() ){
        $records = $connection->prepare('SELECT id_materia,nombre FROM materia;');
        $records->execute();
        $materias = json_encode($records->fetchAll());
        $res = array(
            "status" => 202,
            "message" => "Se eliminó la materia!",
            "materias" => $materias
        );
        echo json_encode($res);
    } else{
        $res = array("status" => 404, "message" => 'No se pudo realizar la operacion. Parece que el servidor esta tenido problemas. Intenta realizar la operacion mas tarde');
        echo json_encode($res);
    }
?>