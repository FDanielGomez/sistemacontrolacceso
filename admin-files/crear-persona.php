<?php
    include('../database.php');
    $new_nombre = $_POST['new_nombre'];
    $new_identificador = $_POST['new_identificador'];
    $new_email = $_POST['new_email'];
    $new_password = $_POST['new_password'];
    $tipo_persona = $_POST['tipo_persona'];
    $records = $connection->prepare('INSERT INTO persona(identificador,email,password,nombre,tipo_persona) VALUES(:new_identificador, :new_email, :new_password, :new_nombre,:tipo_persona);');
    $records->bindParam('new_identificador',$new_identificador);
    $records->bindParam('new_email',$new_email);
    $records->bindParam('new_password',$new_password);
    $records->bindParam('new_nombre',$new_nombre);
    $records->bindParam('tipo_persona',$tipo_persona);
    if($records->execute()) {
        $last_id = $connection->lastInsertId();
        $records = $connection->prepare('SELECT id_persona,identificador,email,nombre FROM persona WHERE id_persona = :id_persona;');
        $records->bindParam('id_persona',$last_id);
        $records->execute();
        $result = $records->fetch(PDO::FETCH_ASSOC);
        $res = array(
            "status" => 202,
            "message" => "Usuario creado exitosamente",
            "id_persona" => $result['id_persona'],
            "identificador" => $result['identificador'],
            "email" => $result['email'],
            "nombre" => $result['nombre'],
        );
        echo json_encode($res);
    }
    else{
        $res = array("status" => 404, "message" => 'No se pudo realizar el registro. Parece que el servidor esta tenido problemas. Intenta realizar la operacion mas tarde');
        echo json_encode($res);
    }
?>