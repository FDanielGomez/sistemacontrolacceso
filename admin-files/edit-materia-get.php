<?php
    include('../database.php');
    $id_materia = $_POST['id_materia'];
    $records = $connection->prepare('SELECT id_materia, nombre FROM materia WHERE id_materia = :id_materia');
    $records->bindParam('id_materia',$id_materia);
    $records->execute();
    $result = $records->fetch(PDO::FETCH_ASSOC);
    echo json_encode($result);  
?>