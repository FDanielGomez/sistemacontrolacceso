<?php
    include('../database.php');
    $id_persona = $_POST['id_persona'];
    $identificador = $_POST['identificador'];
    $email = $_POST['email'];
    $password = $_POST['password'];
    $nombre = $_POST['nombre'];
    $tipo_persona = $_POST['tipo_persona'];
    $records = $connection->prepare('UPDATE persona SET identificador = :identificador, email = :email, password = :password, nombre = :nombre WHERE id_persona = :id_persona;');
    $records->bindParam('identificador',$identificador);
    $records->bindParam('email',$email);
    $records->bindParam('password',$password);
    $records->bindParam('nombre',$nombre);
    $records->bindParam('id_persona',$id_persona);
    if( $records->execute() ){
        $records = $connection->prepare('SELECT id_persona,identificador,nombre,email FROM persona WHERE tipo_persona = :tipo_persona;');
        $records->bindParam('tipo_persona',$tipo_persona);
        $records->execute();
        $personas = json_encode($records->fetchAll());
        $res = array(
            "status" => 202,
            "message" => "Se editaron los datos del usuario exitosamente!",
            "personas" => $personas
        );
        echo json_encode($res);
    } else{
        $res = array("status" => 404, "message" => 'No se pudo realizar el registro. Parece que el servidor esta tenido problemas. Intenta realizar la operacion mas tarde');
        echo json_encode($res);
    }
?>