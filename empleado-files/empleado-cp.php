<?php
  include('../vendor/autoload.php');
    include('../database.php');
    use Endroid\QrCode\QrCode;
    session_start();
    
    date_default_timezone_set('America/Tegucigalpa');
    $date_now = date('d-m-Y');

    $id_cp = $_GET['id_cp'];
    $cp_descripcion = $_GET['cp_descripcion'];
    $records = $connection->prepare('SELECT qrcode FROM punto_control WHERE id_cp = :id_cp;');
    $records->bindParam('id_cp',$id_cp);
    $records->execute();
    $cp_result = $records->fetch(PDO::FETCH_ASSOC);

    $qr_elements = explode('_',$cp_result['qrcode']);
    $fecha_qr = strtotime($qr_elements[1]);
    $fecha_actual = strtotime($date_now);

    if ($fecha_actual > $fecha_qr) {
        $qr_text = $id_cp.'_'.$date_now.'_'.$qr_elements[2];
        $qrCode = new QrCode($qr_text);
        $qrCode->setSize(300);
        //$qrCode->writeFile(__DIR__.'/qrcodes/'.$id_cp.'.png');
        $image = $qrCode->writeString();
        $imageData = base64_encode($image);
        
        $records = $connection->prepare('UPDATE punto_control SET qrcode = :qrcode WHERE id_cp = :id_cp;');
        $records->bindParam('qrcode',$qr_text);
        $records->bindParam('id_cp',$id_cp);
        $records->execute();

        $records = $connection->prepare('UPDATE punto_control SET img_text = :imageData WHERE id_cp = :id_cp;');
        $records->bindParam('imageData',$imageData);
        $records->bindParam('id_cp',$id_cp);
        $records->execute();
    }
    else {
        $records = $connection->prepare('SELECT img_text FROM punto_control WHERE id_cp = :id_cp;');
        $records->bindParam('id_cp',$id_cp);
        $records->execute();
        $data = $records->fetch(PDO::FETCH_ASSOC);
        $imageData = $data['img_text'];
    }
?>
<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>Sistema de control de acceso FI UAEM</title>
  <!-- Custom fonts for this template-->
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"/>
  <link href="../fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link rel="icon" type="image/png" href="../favicon.png"/>
  <!-- Custom styles for this template-->
  <link href="../css/standard-style.css" rel="stylesheet">
  <link rel="stylesheet" href="../css/empleado-style.css">
</head>
<body id="page-top">
  <!-- Page Wrapper -->
  <div id="wrapper">
    <!-- Sidebar -->
    <ul class="navbar-nav sidebar accordion" id="accordionSidebar">
      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="">
        <div class="sidebar-brand-icon">
          <img src="/sistemaAccesoFI/img/png/uaem-logo.png" alt="Universidad Autonoma de Estado de Mexico" width="57px" height="50px" style="border-radius:3px;">
        </div>
        <div class="sidebar-brand-text mx-2">UAEM</div>
      </a>
      <!-- Divider -->
      <hr class="sidebar-divider my-0">
      <!-- Nav Item - Dashboard -->
      <li class="nav-item active">
        <a class="nav-link" href="../empleado.php">
          <ion-icon name="contract-outline" style="font-size: 22px; vertical-align: middle; padding-right:5px;"></ion-icon>
          <span>Puntos de control</span>
        </a>
      </li>
      <!-- Divider -->
      <hr class="sidebar-divider">
      <!-- Heading -->
      <div class="sidebar-heading">
        Operaciones
      </div>
      <!-- Nav Item -->
      <li class="nav-item">
        <a class="nav-link" href="empleado-registrar.php">
        <ion-icon name="person-add-outline" style="font-size: 22px; vertical-align: middle; padding-right:5px;"></ion-icon>
          <span>Registrar acceso</span>
        </a>
      </li>
      <!-- Divider -->
      <hr class="sidebar-divider d-none d-md-block">
      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>
    </ul>
    <!-- End of Sidebar -->
    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">
      <!-- Main Content -->
      <div id="content">

      <?php include($_SERVER['DOCUMENT_ROOT'].'/sistemaAccesoFI/templates/navbar.php'); ?>
      
        <!-- Begin Page Content -->
        <div class="container-fluid">
           <!-- Page Heading -->
           <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="../empleado.php">Puntos de control</a></li>
                  <li class="breadcrumb-item active" aria-current="page"><a>Detalles de punto de control</a></li>
                </ol>
            </nav>
            <div class="row mb-4">
                <div class="col-md-9 mx-auto">
                    <h3 class="mt-4">Código QR del punto de control</h3>
                    <div class="card card-content shadow mt-4">
                        <div class="card-body">
                            <div class="row justify-content-between">
                                <div class="col d-flex flex-column justify-content-center my-4">
                                    <strong class="p-0 mb-2 mx-auto" style="color: #1C3D14;">Punto de control:</strong>
                                    <h4 class="mx-auto p-0 m-0 mb-4"> <?=$cp_descripcion?></h4>
                                    <!--<button class="btn btn-hov mb-2 mx-auto" style="max-width:60%;"><ion-icon name="reload-outline" style="font-size: 20px; vertical-align: middle;"></ion-icon>&nbsp; VOLVER A GENERAR</button>-->
                                    <a href="../get-pdf.php?id_cp=<?= $id_cp?>" class="btn btn-hov mb-2 mx-auto" style="max-width:100%;">
                                      <!--<ion-icon name="download-outline"></ion-icon>-->
                                      <ion-icon name="document-outline" style="font-size: 22px; vertical-align: middle;"></ion-icon>
                                      &nbsp; DESCARGAR PDF
                                    </a>
                                </div>
                                <div class="col d-flex flex-column justify-content-center p-0 my-2">
                                    <strong class="mb-2 p-0 mx-auto" style="color: #1C3D14;">Código QR:</strong>
                                    <img class="mx-auto" src="data:image/png;base64,<?=$imageData?>" width="250" height="250" alt="QR Code">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End of container-fluid -->
      </div>
      <!-- End of Main Content -->
    </div>
    <!-- End of Content Wrapper -->
  </div>
  <!-- End of Page Wrapper -->
  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>
  
  <?php include('../templates/logout-modal.php'); ?>

  <script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
  <script src="https://unpkg.com/ionicons@5.2.3/dist/ionicons.js"></script>
  <!-- Custom scripts for all pages-->
  <script src="../js/standard-func.js"></script>
</body>
</html>
