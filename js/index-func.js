$(document).ready(function () {
    $(document).on('scroll', function() {
        var scrollDistance = $(this).scrollTop();
        if (scrollDistance > 140) {
        $('.scroll-to-top').fadeIn();
        } else {
        $('.scroll-to-top').fadeOut();
        }
    });
        
    $('.scroll-to-top').click(function(){
        $('html, body').animate({
            scrollTop: '0'
        }, 300);
    });
});