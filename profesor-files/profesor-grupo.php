<?php
  include('../database.php');
  session_start();
  $id_grupo = $_GET['id_grupo'];
  $nombre_materia = $_GET['nombre_materia'];
  $index = 0;
  date_default_timezone_set('America/Tegucigalpa');
  $fecha = date('d/m/Y');
  $fecha_actual = date('Y-m-d');
  $records = $connection->prepare('SELECT persona.id_persona,persona.nombre FROM alumno_grupo, persona WHERE alumno_grupo.id_alumno = persona.id_persona AND alumno_grupo.id_grupo = :id_grupo ORDER BY nombre;');
  $records->bindParam('id_grupo',$id_grupo);
  $records->execute();
  $alumnos = $records->fetchAll();
  $records = $connection->prepare('SELECT persona.id_persona,persona.nombre,DATE_FORMAT(acceso.registered_at,\'%H:%i:%s\') AS hora_reg,DATE_FORMAT(acceso.registered_at,\'%d/%m/%Y\') AS fecha_reg,punto_control.descripcion FROM alumno_grupo, persona,acceso,punto_control WHERE alumno_grupo.id_alumno = persona.id_persona  AND persona.id_persona = acceso.id_persona AND acceso.id_cp = punto_control.id_cp AND alumno_grupo.id_grupo = :id_grupo AND acceso.registered_at >= :fecha_actual GROUP BY persona.id_persona ORDER BY nombre;');
  $records->bindParam('id_grupo',$id_grupo);
  $records->bindParam('fecha_actual',$fecha_actual);
  $records->execute();
  $alumnos_acceso = $records->fetchAll();
?>
<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>Profesor - Sistema de control de acceso FI UAEM</title>
  <!-- Custom fonts for this template-->
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"/>
  <link href="../fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link rel="icon" type="image/png" href="../favicon.png"/>
  <!-- Custom styles for this template-->
  <link href="../css/standard-style.css" rel="stylesheet">
  <link rel="stylesheet" href="../css/profesor-style.css">
</head>
<body id="page-top">
  <!-- Page Wrapper -->
  <div id="wrapper">
    <!-- Sidebar -->
    <ul class="navbar-nav sidebar accordion" id="accordionSidebar">
      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="">
        <div class="sidebar-brand-icon">
          <img src="/sistemaAccesoFI/img/png/uaem-logo.png" alt="" width="57px" height="50px" style="border-radius:3px;">
        </div>
        <div class="sidebar-brand-text mx-2">UAEM</div>
      </a>
      <!-- Divider -->
      <hr class="sidebar-divider my-0">
      <!-- Nav Item - Dashboard -->
      <li class="nav-item active">
        <a class="nav-link" href="../profesor.php">
          <ion-icon name="people-outline" style="font-size: 22px; vertical-align: middle; padding-right:5px;"></ion-icon>
          <span>Grupos</span></a>
      </li>
      <!-- Divider -->
      <hr class="sidebar-divider d-none d-md-block">
      <!-- Heading -->
      <div class="sidebar-heading">
        Ayuda
      </div>
      <!-- Nav Item - Tables -->
      <li class="nav-item">
        <a class="nav-link" href="">
          <ion-icon name="help-circle-outline" style="font-size: 22px; vertical-align: middle; padding-right:5px;"></ion-icon>
          <span>Ayuda</span>
        </a>
      </li>
      <!-- Divider -->
      <hr class="sidebar-divider d-none d-md-block">
      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

    </ul>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">
      <!-- Main Content -->
      <div id="content">
        <?php include($_SERVER['DOCUMENT_ROOT'].'/sistemaAccesoFI/templates/navbar.php'); ?>
        <!-- Begin Page Content -->
        <div class="container-fluid">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="../profesor.php">Grupos</a></li>
                  <li class="breadcrumb-item active" aria-current="page"><a><?=$nombre_materia?></a></li>
                </ol>
            </nav>
          <!-- Page Heading -->
          <div class="row">
            <div class="col-md-11 mx-auto">
              <h1 class="h3 my-4">Grupo: <?=$nombre_materia?></h1>
              <p>Estatus de los alumnos para el dia de hoy <?=$fecha?>.</p>
                <div class="accordion shadow mb-5" id="accordionExample">
                <?php if(!empty($alumnos_acceso)): ?>
                  <?php for ($i = 0; $i < sizeof($alumnos); $i++) {?>    
                    <?php if( ($index < sizeof($alumnos_acceso)) && ($alumnos[$i]['id_persona'] == $alumnos_acceso[$index]['id_persona'])): ?>
                      <div class="card py-2 border-2">
                          <h2 class="mb-0">
                            <button class="btn btn-link btn-block btn-list text-left collapsed" type="button" data-toggle="collapse" data-target="#collapse<?=$alumnos_acceso[$index]['id_persona']?>" aria-expanded="false" aria-controls="collapse<?=$alumnos_persona[$index]['id_persona']?>" style="text-decoration: none;">
                              <span class="badge badge-pill badge-success"><i class="fas fa-check"></i><span class="d-none d-sm-inline">&nbsp; &nbsp;Con acceso</span></span>&nbsp; &nbsp;
                              <span class="text-decoration-none" style="text-decoration: none;"> <?=$alumnos_acceso[$index]['nombre']?></span>
                            </button>
                          </h2>
                        <div id="collapse<?=$alumnos_acceso[$index]['id_persona']?>" class="collapse bg-light m-0 p-0" aria-labelledby="headingOne" data-parent="#accordionExample">
                          <div class="card-body">
                              <p style="font-size: 14px;"><strong>Hora de registro:</strong> <?=$alumnos_acceso[$index]['hora_reg']?></p>
                              <p style="font-size: 14px;"><strong>Fecha de registro:</strong> <?=$alumnos_acceso[$index]['fecha_reg']?></p>
                              <p style="font-size: 14px;"><strong>Punto de control:</strong> <?=$alumnos_acceso[$index]['descripcion']?></p>
                          </div>
                        </div>
                      </div>
                      <?php $index = $index+1; ?>
                    <?php else: ?>
                      <div class="card py-2 border-2">
                        <h2 class="mb-0">
                          <button class="btn btn-link btn-block btn-list text-left collapsed" type="button" data-toggle="collapse" data-target="#collapse<?=$alumnos[$i]['id_persona']?>" aria-expanded="false" aria-controls="collapse<?=$alumnos[$i]['id_persona']?>" style="text-decoration: none;">
                            <span class="badge badge-pill badge-danger"><i class="fas fa-times"></i><span class="d-none d-sm-inline">&nbsp; &nbsp;Sin acceso</span></span>&nbsp; &nbsp;
                            <span class="text-decoration-none" style="text-decoration: none;"> <?=$alumnos[$i]['nombre']?></span>
                          </button>
                        </h2>
                        <div id="collapse<?=$alumnos[$i]['id_persona']?>" class="collapse bg-light m-0 p-0" aria-labelledby="headingOne" data-parent="#accordionExample">
                          <div class="card-body">
                              <p style="font-size: 14px;"><strong><?=$alumnos[$i]['nombre']?> no ha pasado por ningun punto de control.</strong></p>
                          </div>
                        </div>
                      </div>
                    <?php endif; ?>
                  <?php };?>  
                <?php else: ?>
                  <?php for ($i = 0; $i < sizeof($alumnos); $i++) {?> 
                    <div class="card py-2 border-2">
                        <h2 class="mb-0">
                          <button class="btn btn-link btn-block btn-list text-left collapsed" type="button" data-toggle="collapse" data-target="#collapse<?=$alumnos[$i]['id_persona']?>" aria-expanded="false" aria-controls="collapse<?=$alumnos[$i]['id_persona']?>" style="text-decoration: none;">
                            <span class="badge badge-pill badge-danger"><i class="fas fa-times"></i><span class="d-none d-sm-inline">&nbsp; &nbsp;Sin acceso</span></span>&nbsp; &nbsp;
                            <span class="text-decoration-none" style="text-decoration: none;"> <?=$alumnos[$i]['nombre']?></span>
                          </button>
                        </h2>
                      <div id="collapse<?=$alumnos[$i]['id_persona']?>" class="collapse bg-light m-0 p-0" aria-labelledby="headingOne" data-parent="#accordionExample">
                        <div class="card-body">
                            <p style="font-size: 14px;"><strong><?=$alumnos[$i]['nombre']?> no ha pasado por ningun punto de control.</strong></p>
                        </div>
                      </div>
                    </div>
                  <?php };?>
                <?php endif; ?>
                </div>
            </div>
          </div>
        </div>
        <!-- /.container-fluid -->
      </div>
      <!-- End of Main Content -->
    </div>
    <!-- End of Content Wrapper -->
  </div>
  <!-- End of Page Wrapper -->
  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <?php include('../templates/logout-modal.php'); ?>

  <script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
  <script src="https://unpkg.com/ionicons@5.2.3/dist/ionicons.js"></script>
  <!-- Custom scripts for all pages-->
  <script src="../js/standard-func.js"></script>
</body>
</html>
